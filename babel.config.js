module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  plugins: [
    [
      // "module:react-native-dotenv",
      "module-resolver",
      {
        // moduleName: "@env",
        // path: ".env",
        // blacklist: null,
        // whitelist: null,
        // safe: false,
        // allowUndefined: true,
        root: ['./src'],
        alias: {
          '@assets': './src/assets',
          // '@icons': './src/assets/icons',
          '@components': './src/components',
          // '@instances': './src/instances',
          // '@interfaces': './src/interfaces',
          '@navigation': './src/navigation',
          '@redux': './src/redux',
          '@screens': './src/screens',
          '@services': './src/services',
          // '@firebase': './src/firebaseNotification',
          // '@theme': './src/theme',
        },
      },
    ],
  ],
};
