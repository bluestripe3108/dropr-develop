//
//  TPSStripeManager.h
//  Dropr_app
//
//  Created by Hieu Pham on 06/04/2022.
//

#import <Foundation/Foundation.h>
#import <PassKit/PassKit.h>
@import Stripe;
#import <React/RCTBridgeModule.h>
#import <React/RCTConvert.h>

@interface StripeModule : NSObject <RCTBridgeModule, PKPaymentAuthorizationViewControllerDelegate, STPAddCardViewControllerDelegate>

@property (nonatomic) STPRedirectContext *redirectContext;

@end

