import React from "react";
import { Provider } from "react-redux";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/es/integration/react";
import FlashMessage from "react-native-flash-message";
import { StripeProvider } from "@stripe/stripe-react-native";

import { GlobalUI } from "./src/components/GlobalUI";
import { GlobalService } from "./src/services/GlobleUIService";
import store from "./src/redux/store";
import {StackNavigation} from "@navigation";
// const publishKey =
//   "pk_test_51LD2urGKS7nkfxKBeylGgutAasDKY5V7eqikeb8CEMsaX8Utax6WrSABEyIRzdMSZXgXJ628j7XahkE9HEz2ClNs00CJpgFUZV";

const publishKey =
  "pk_test_51L6AWmGROtrteUVR8vR4WLpcNT1KFiHJjWYjGYiYB0Fwh0HdRDkbj9fy4Hlp88z1S76TufwTL755QUnWswWUUhOT00UijEYwS3";
const App = () => {
  const persistor = persistStore(store);

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <StripeProvider publishableKey={publishKey}>
          <StackNavigation />
        </StripeProvider>

        <FlashMessage position="top" floating={true} hideStatusBar={false} />
        <GlobalUI ref={GlobalService.globalUIRef} />
      </PersistGate>
    </Provider>
  );
};

export default App;
